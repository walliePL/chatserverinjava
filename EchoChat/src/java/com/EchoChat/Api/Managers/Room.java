package com.EchoChat.Api.Managers;

import com.EchoChat.Api.Components.ChatMessage;
import com.EchoChat.Api.Components.ChatMessageEntity;
import com.EchoChat.Api.Components.ChatMessageMapper;
import com.EchoChat.Api.Components.User;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;
import static java.util.logging.Level.SEVERE;
import java.util.logging.Logger;
import javax.enterprise.context.ApplicationScoped;
import javax.websocket.EncodeException;
import javax.websocket.Session;

@ApplicationScoped
public class Room  {
    private String name;
    private final Set<User> users = new HashSet<>();
    private final List<ChatMessageEntity> history = new LinkedList<>();  
    private final Queue<ChatMessageEntity> recentHisotry = new LinkedList<ChatMessageEntity>();

    public Set<User> getUsers() {
        return users;
    }

    public Room(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void addUser(User newUser) {
        this.users.add(newUser);
    }

    public List<ChatMessageEntity> getHistory() {
        return history;
    }

    public void removeUser(User user) {
        if (this.users.contains(user)) {
            this.users.remove(user);
        }
    }
    
    public User getUserByName(String name) {
        return this.users.stream().filter(user -> user.getName().equals(name)).findFirst().get();
    }
    
    public boolean hasUser(String name){
        return this.users.stream().anyMatch(user -> user.getName().equals(name));
    }
    
    public void sendMessage(final String messageToSend, User user)
    {
        Session userSession = user.getSession();
        if (userSession.isOpen()) {
                try {
                   userSession.getBasicRemote().sendObject(messageToSend);
                } catch (IOException | EncodeException ex) {
                    Logger.getLogger(Room.class.getName()).log(SEVERE, null, ex);
                }
        }
    }
    
    public void sendMessage(final ChatMessageEntity chatMessageEntity, User user)
    {
        String messageToSend = ChatMessageMapper.createOutputMessage(chatMessageEntity);
        sendMessage(messageToSend, user);
    }
    
    public void sendHisotryToUser(User user)
    {
        recentHisotry.forEach(message -> sendMessage(message, user));
    }
    
    public void broadcastMessage(final ChatMessage chatMessage, String transmitterName)
    {
        String messageToBroadcast = ChatMessageMapper.createOutputMessage(chatMessage, transmitterName);
        for (User user : this.users) {
            sendMessage(messageToBroadcast, user);
        }
        
        if (!chatMessage.equals("")) {
            final ChatMessageEntity chatMessageEntity = new ChatMessageEntity(transmitterName, chatMessage.getMessage(), chatMessage.getReceived());
            history.add(chatMessageEntity);
            
            if (recentHisotry.size() > 100) recentHisotry.poll();
            recentHisotry.offer(chatMessageEntity);
        }
    }
}
