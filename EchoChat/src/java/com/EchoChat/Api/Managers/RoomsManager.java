/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.EchoChat.Api.Managers;

import java.util.HashSet;
import java.util.Set;
import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class RoomsManager {
    private final Set<Room> rooms = new HashSet<>();   

    public RoomsManager() {
        rooms.add(new Room("main"));
    }
    
    public Set<Room> getRooms() {
        return rooms;
    }
    
    public Room getRoomFromName(String name){
        return rooms.stream().filter(room -> room.getName().equals(name)).findFirst().get();
    }
    
}
