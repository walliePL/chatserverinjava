/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.EchoChat.Api.Components;

import java.io.StringReader;
import java.util.Date;
import javax.json.Json;
import javax.json.JsonObject;
import javax.websocket.EncodeException;
public class ChatMessageMapper  {

    public static ChatMessage decodeIncommingMessage(String message) {
  
        ChatMessage chatMessage = new ChatMessage();
        JsonObject obj = Json.createReader(new StringReader(message))
                .readObject();
        chatMessage.setMessage(obj.getString("message"));
        chatMessage.setReceived(new Date());
        return chatMessage;
    }
    
    public static String createOutputMessage(ChatMessage message, String username) {
        return Json.createObjectBuilder()
				.add("message", message.getMessage())
				.add("username", username)
				.add("received", message.getReceived().toString()).build()
				.toString();
    }  
    
    public static String createOutputMessage(ChatMessageEntity message) {
        return Json.createObjectBuilder()
				.add("message", message.getMessage())
				.add("username", message.getUsername())
				.add("received", message.getReceived().toString()).build()
				.toString();
    }  
}
