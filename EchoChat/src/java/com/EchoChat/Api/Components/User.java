package com.EchoChat.Api.Components;

import java.io.Serializable;
import javax.enterprise.context.SessionScoped;
import javax.websocket.Session;

public class User implements Serializable {
    private String name;
    private String email;
    private Session session;
   
    private User(String name, String email, Session session) {
        this.name = name;
        this.email = email;
        this.session = session;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }
    
    public static User createUser(String name, String email, Session session) {
        return new User(name, email, session);
    }
}
