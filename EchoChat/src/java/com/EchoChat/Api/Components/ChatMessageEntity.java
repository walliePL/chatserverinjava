/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.EchoChat.Api.Components;

import java.util.Date;

public class ChatMessageEntity {
    private String username;
    private String message;
    private Date received;

    public ChatMessageEntity(String username, String message, Date received) {
        this.username = username;
        this.message = message;
        this.received = received;
    }
    
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getReceived() {
        return received;
    }

    public void setReceived(Date received) {
        this.received = received;
    }
}
