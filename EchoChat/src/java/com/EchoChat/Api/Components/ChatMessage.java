/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.EchoChat.Api.Components;

import java.util.Date;

public class ChatMessage {
    private String message;
    private Date received;

    public ChatMessage(String message, Date received) {
        this.message = message;
        this.received = received;
    }

    public ChatMessage() {
    }
    
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getReceived() {
        return received;
    }

    public void setReceived(Date recived) {
        this.received = recived;
    }
}

