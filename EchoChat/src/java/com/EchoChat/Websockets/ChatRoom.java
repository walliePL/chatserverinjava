/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.EchoChat.Websockets;

import com.EchoChat.Api.Components.ChatMessage;
import com.EchoChat.Api.Components.ChatMessageDecoder;
import com.EchoChat.Api.Components.ChatMessageEncoder;
import com.EchoChat.Api.Components.User;
import com.EchoChat.Api.Managers.ChatRoomManager;
import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.websocket.EncodeException;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

@ServerEndpoint(value = "/rooms/main", encoders = ChatMessageEncoder.class, decoders = ChatMessageDecoder.class)
public class ChatRoom {

    @Inject
    private ChatRoomManager chatRoomManager;
    
    @OnMessage
    public void onMessage(final Session session, final ChatMessage chatMessage) {
        
        for (User user : chatRoomManager.getUsers()) {
            Session userSession = user.getSession();
            if (userSession.isOpen()) {
                try {
                    userSession.getBasicRemote().sendObject(chatMessage);
                } catch (IOException | EncodeException ex) {
                    Logger.getLogger(ChatRoom.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    
    @OnOpen
    public void open(Session session) {
        // prompt for user and passwd
    }
    
    @OnClose
    public void close(Session session) {
    }
         
    
}
