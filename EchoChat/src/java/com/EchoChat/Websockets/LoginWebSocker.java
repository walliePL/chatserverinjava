/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.EchoChat.Websockets;

import com.EchoChat.Api.Components.ChatMessage;
import com.EchoChat.Api.Components.ChatMessageMapper;
import com.EchoChat.Api.Components.User;
import com.EchoChat.Api.Managers.Room;
import com.EchoChat.Api.Managers.RoomsManager;
import java.io.StringReader;
import java.util.Date;
import static java.util.logging.Level.SEVERE;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;

@ServerEndpoint("/rooms/{room}")
public class LoginWebSocker {
    private Logger log = Logger.getLogger(LoginWebSocker.class.getSimpleName());

    @Inject
    private RoomsManager roomsManager;
    
    @OnOpen
    public void open(Session session, @PathParam("room") final String room) {
        session.getUserProperties().put("room", room); //todo: if room has a bad name retrun error
    }
    
    @OnClose
    public void close(Session session) {
        Logout(session);
    }

    @OnError
    public void onError(Throwable error) {
        Logger.getLogger(LoginWebSocker.class.getName()).log(SEVERE, null, error);
    }
    
    @OnMessage
    public void onMessage(String message, Session session) {
        Room room = findRoom(session);
        
        try (JsonReader reader = Json.createReader(new StringReader(message))) {
            log.info(message);

            JsonObject jsonMessage = reader.readObject();

            if ("LOGIN".equals(jsonMessage.getString("action"))) {
                String username = jsonMessage.getString("username");
                String email = jsonMessage.getString("email");
                session.getUserProperties().put("username", username);
                User user = User.createUser(username, email, session);
                log.info("User: " + username + " login");
                room.addUser(user);
                
                room.sendHisotryToUser(user);
                
                ChatMessage chatMessage = new ChatMessage("USER: " + username + " LOGIN", new Date());
                room.broadcastMessage(chatMessage, "");
                
                return;
            }
            
            String username = (String) session.getUserProperties().get("username");

            if ("LOGOUT".equals(jsonMessage.getString("action"))) {
                if (room.hasUser(username))
                {
                    Logout(session);
                }
                return;
            }

            if ("MESSAGE".equals(jsonMessage.getString("action"))) {
                if (room.hasUser(username))
                {
                    room.broadcastMessage(ChatMessageMapper.decodeIncommingMessage(message), username);
                }
            }
        }
    }

    private Room findRoom(Session session) {
        String roomName = (String) session.getUserProperties().get("room");
        Room room = roomsManager.getRoomFromName(roomName);
        return room;
    }
    
    private void Logout(Session session) {
        Room room = findRoom(session);

        String username = (String) session.getUserProperties().get("username");
        log.info("User: " + username + " logout");
        room.removeUser(room.getUserByName(username));
        
        ChatMessage chatMessage = new ChatMessage("USER: " + username + " LOGOUT", new Date());
        room.broadcastMessage(chatMessage, "");
    }
}
