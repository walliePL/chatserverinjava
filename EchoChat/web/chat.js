/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var chatsocket;
var serviceLocation = "ws://127.0.0.1:8080/EchoChat/rooms/";
var $nickName;
var $email;
var $message;
var $chatWindow;
var room = '';

function onMessageReceived(evt) {
	//var msg = eval('(' + evt.data + ')');
	var msg = JSON.parse(evt.data); // native API
	var $messageLine = $('<tr><td class="received">' + msg.received
			+ '</td><td class="user label label-info">' + msg.username
			+ '</td><td class="message badge">' + msg.message
			+ '</td></tr>');
	$chatWindow.append($messageLine);
}
function sendMessage() {
	var msg = '{"message":"' + $message.val() + '", "username":"'
			+ $nickName.val() + '", "received":"" , "action":"MESSAGE"}';
	chatsocket.send(msg);
	$message.val('').focus();
}

function connectToChatserver() {
        var msg = '{"email":  "'
			+ $email.val() + '", "username":"'
			+ $nickName.val() + '", "action":"LOGIN"}';
        chatsocket = new WebSocket(serviceLocation + room);
        chatsocket.onmessage = onMessageReceived;
        send(msg);
}

function enterRoom() {
    	room = $('#chatroom option:selected').val();
}

function leaveRoom() {
	chatsocket.close();
	$chatWindow.empty();
        $('.chat-SelectRoomPanel').show();
	$('.chat-wrapper').hide();
	$('.chat-signin').hide();
	$nickName.focus();
}

$(document).ready(function() {
	$nickName = $('#nickname');
        $email = $('#email');
	$message = $('#message');
	$chatWindow = $('#response');
	$('.chat-wrapper').hide();
        $('.chat-signin').hide();

	$nickName.focus();

        $('#enter-room').click(function(evt) {
		evt.preventDefault();
		enterRoom();
		$('.chat-SelectRoomPanel').hide();
		$('.chat-signin').show();
	});
	$('#login').click(function(evt) {
		evt.preventDefault();
		connectToChatserver();
                $('.chat-wrapper h2').text('Chat # '+$nickName.val() + "@" + room);
		$('.chat-SelectRoomPanel').hide();
		$('.chat-signin').hide();
                $('.chat-wrapper').show();
                $message.focus();
	});
	$('#do-chat').submit(function(evt) {
		evt.preventDefault();
		sendMessage()
	});

	$('#leave-room').click(function(){
		leaveRoom();
	});
});

function send(message, callback) {
    this.waitForConnection(function () {
        chatsocket.send(message);
        if (typeof callback !== 'undefined') {
          callback();
        }
    }, 1000);
};

function waitForConnection (callback, interval) {
    if (chatsocket.readyState === 1) {
        callback();
    } else {
        var that = this;
        // optional: implement backoff for interval here
        setTimeout(function () {
            that.waitForConnection(callback, interval);
        }, interval);
    }
};

