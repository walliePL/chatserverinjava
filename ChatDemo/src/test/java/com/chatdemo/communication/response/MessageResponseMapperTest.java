package com.chatdemo.communication.response;

import com.chatdemo.entities.MessageEntity;
import org.testng.annotations.Test;

import java.util.Date;

import static org.testng.Assert.assertEquals;

public class MessageResponseMapperTest {

    public MessageResponseMapperTest() {
    }

    @Test
    public void testMapToEntity() {
        String username = "usernameTest";
        String message = "messageTest";
        String roomName = "roomNameTest";
        Date received = new Date();

        MessageResponse messageResponse = new MessageResponse(username, message, received);
        MessageEntity messageEntity = MessageResponseMapper.mapToEntity(messageResponse, roomName);

        assertEquals(messageEntity.getMessage(), message);
        assertEquals(messageEntity.getReceived(), received);
        assertEquals(messageEntity.getRoomName(), roomName);
        assertEquals(messageEntity.getUsername(), username);
    }

}
