package com.chatdemo.communication.response;

import org.testng.annotations.Test;

import javax.json.Json;
import javax.json.JsonObject;
import java.util.Date;

import static org.testng.Assert.assertEquals;

public class MessageResponseTest {

    public MessageResponseTest() {
    }

    @Test
    public void testGetJsonResponse() {
        String message = "message";
        String username = "username";
        Date date = new Date();

        MessageResponse instance = new MessageResponse(username, message, date);
        JsonObject expResult = Json.createObjectBuilder()
                .add("type", "Message")
                .add("message", message)
                .add("username", username)
                .add("received", date.toString()).build();
        JsonObject result = instance.getJsonResponse();
        assertEquals(result.toString(), expResult.toString());
    }

}
