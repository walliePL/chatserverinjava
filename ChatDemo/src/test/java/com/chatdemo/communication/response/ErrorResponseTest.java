package com.chatdemo.communication.response;

import org.testng.annotations.Test;

import javax.json.Json;
import javax.json.JsonObject;

import static org.testng.Assert.assertEquals;

public class ErrorResponseTest {

    public ErrorResponseTest() {
    }

    @Test
    public void testGetJsonResponse() {
        String errorMessage = "errorMessage";
        ErrorResponse instance = new ErrorResponse(errorMessage);
        JsonObject expResult = Json.createObjectBuilder()
                .add("type", "Error")
                .add("errorMessage", errorMessage).build();
        JsonObject result = instance.getJsonResponse();
        assertEquals(result.toString(), expResult.toString());
    }

}
