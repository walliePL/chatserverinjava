package com.chatdemo.utils;

import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class UserValidatorNGTest {

    public UserValidatorNGTest() {
    }

    @Test
    public void testEmptyUsername() {
        String username = "";
        boolean expResult = false;
        boolean result = UserValidator.isValidUsername(username);
        assertEquals(result, expResult);
    }

    @Test
    public void testProperUsername() {
        String username = "username";
        boolean expResult = true;
        boolean result = UserValidator.isValidUsername(username);
        assertEquals(result, expResult);
    }

    @Test
    public void testInvalidateUsername() {
        String username = "username!@";
        boolean expResult = false;
        boolean result = UserValidator.isValidUsername(username);
        assertEquals(result, expResult);
    }

    @Test
    public void testUsernameWithOnlyNumbers() {
        String username = "2";
        boolean expResult = true;
        boolean result = UserValidator.isValidUsername(username);
        assertEquals(result, expResult);
    }

    @Test
    public void testEmptyEmailAddress() {
        String email = "";
        boolean expResult = false;
        boolean result = UserValidator.isValidEmailAddress(email);
        assertEquals(result, expResult);
    }

    @Test
    public void testProperEmailAddress() {
        String email = "xxx@ls.pl";
        boolean expResult = true;
        boolean result = UserValidator.isValidEmailAddress(email);
        assertEquals(result, expResult);
    }

    @Test
    public void testInvalidEmailAddress() {
        String email = "xxx@ls";
        boolean expResult = false;
        boolean result = UserValidator.isValidEmailAddress(email);
        assertEquals(result, expResult);

        email = "xxx@";
        result = UserValidator.isValidEmailAddress(email);
        assertEquals(result, expResult);

        email = "xxxss.cs";
        result = UserValidator.isValidEmailAddress(email);
        assertEquals(result, expResult);
    }

}
