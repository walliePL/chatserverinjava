package com.chatdemo.services;

import com.chatdemo.communication.response.ErrorResponse;

import javax.websocket.EncodeException;
import javax.websocket.Session;
import java.io.IOException;
import java.util.logging.Logger;

import static java.util.logging.Level.SEVERE;

public class SendService {
    private static final Logger log = Logger.getLogger(SendService.class.getSimpleName());

    public static void sendErrorToSession(Session session, String message) {
        ErrorResponse errorResponse = new ErrorResponse(message);
        try {
            session.getBasicRemote().sendObject(errorResponse.getJsonResponse().toString());
        } catch (IOException | EncodeException sendObjectException) {
            log.log(SEVERE, "Cannot send error response to session");
        }
    }
}
