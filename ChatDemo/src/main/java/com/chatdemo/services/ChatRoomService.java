package com.chatdemo.services;

import com.chatdemo.communication.request.MessageRequest;
import com.chatdemo.communication.response.MessageResponse;
import com.chatdemo.room.Room;
import com.chatdemo.room.RoomsHolder;
import com.chatdemo.room.User;
import com.chatdemo.utils.UserValidator;
import com.chatdemo.utils.exceptions.RoomException;
import com.chatdemo.utils.exceptions.RoomServiceException;

import javax.ejb.DependsOn;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import javax.json.JsonObject;
import javax.websocket.Session;
import java.util.Date;
import java.util.logging.Logger;

import static java.util.logging.Level.INFO;
import static java.util.logging.Level.SEVERE;

@DependsOn("RoomsHolder")
@Startup
@Singleton
public class ChatRoomService {
    public static final String CHAT_LOG_USERNAME = "";
    private static final Logger log = Logger.getLogger(ChatRoomService.class.getSimpleName());
    @Inject
    private RoomsHolder roomsManager;

    public void login(JsonObject jsonMessage, Session session) throws RoomServiceException {
        Room room = findRoom(session);
        String username = jsonMessage.getString("username");
        if (!UserValidator.isValidUsername(username)) {
            SendService.sendErrorToSession(session, "Malformed username");
            return;
        }
        String email = jsonMessage.getString("email");
        if (!UserValidator.isValidEmailAddress(email)) {
            SendService.sendErrorToSession(session, "Malformed email address");
            return;
        }
        session.getUserProperties().put("username", username);
        User user = new User(username, email, session);
        addUserToRoom(room, user, username);
        log.log(INFO, "User: {0} login", username);
    }

    public void logout(Session session) throws RoomServiceException {
        Room room = findRoom(session);
        String username = (String) session.getUserProperties().get("username");
        if (!room.hasUser(username)) {
            SendService.sendErrorToSession(session, "You are not logged in this room");
            return;
        }
        User user = room.getUserByName(username);
        removeUserFromRoom(room, user, username);
        log.log(INFO, "User: {0} logout", username);
    }

    public void processChatMessage(Session session, JsonObject jsonMessage) throws RoomServiceException {
        Room room = findRoom(session);

        String username = (String) session.getUserProperties().get("username");
        if (!room.hasUser(username)) {
            SendService.sendErrorToSession(session, "You are not logged in this room");
            return;
        }
        MessageRequest messageRequest = new MessageRequest();
        messageRequest.mapRequest(jsonMessage);
        room.receiveMessage(messageRequest, username);
    }

    private Room findRoom(Session session) throws RoomServiceException {
        String roomName = (String) session.getUserProperties().get("room");
        Room room = roomsManager.getRoomFromName(roomName);
        if (room == null) {
            throw new RoomServiceException("Cannot find room");
        }
        return room;
    }

    private void informAboutLogout(Room room, String username) {
        MessageResponse leftMessage = new MessageResponse(CHAT_LOG_USERNAME, "user: " + username + " left room", new Date());
        room.broadcastResponse(leftMessage);
    }

    private void addUserToRoom(Room room, User user, String username) {
        room.addUser(user);
        room.sendHistoryToUser(user);
        MessageResponse joinMessage = new MessageResponse(CHAT_LOG_USERNAME, "user: " + username + " join", new Date());
        room.broadcastResponse(joinMessage);
    }

    private void removeUserFromRoom(Room room, User user, String username) {
        try {
            room.removeUser(user);
        } catch (RoomException ex) {
            log.log(SEVERE, null, ex);
        }
        informAboutLogout(room, username);
    }
}
