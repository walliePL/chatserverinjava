package com.chatdemo.room;

import com.chatdemo.communication.request.MessageRequest;
import com.chatdemo.communication.response.ChatResponse;
import com.chatdemo.communication.response.MessageResponse;
import com.chatdemo.communication.response.MessageResponseMapper;
import com.chatdemo.entities.MessageDao;
import com.chatdemo.utils.exceptions.RoomException;

import javax.websocket.EncodeException;
import javax.websocket.Session;
import java.io.IOException;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.logging.Logger;

import static com.chatdemo.services.ChatRoomService.CHAT_LOG_USERNAME;
import static java.util.logging.Level.SEVERE;

public class Room {
    private final String roomName;
    private final Set<User> loggedInUsers = new HashSet<>();
    private final ChatHistory chatHistory = new ChatHistory();
    private final MessageDao messageDao = new MessageDao();
    private final Logger log = Logger.getLogger(Room.class.getSimpleName());

    public Room(String name) {
        this.roomName = name;
    }

    public Set<User> getUsers() {
        return loggedInUsers;
    }

    public String getName() {
        return roomName;
    }

    public void addUser(User newUser) {
        this.loggedInUsers.add(newUser);
    }

    public void removeUser(User user) throws RoomException {
        if (this.loggedInUsers.contains(user)) {
            this.loggedInUsers.remove(user);
        } else {
            throw new RoomException("Removing not loged user: " + user.getName() + " from room");
        }
    }

    public User getUserByName(String name) {
        return this.loggedInUsers.stream().filter(user -> user.getName().equals(name)).findFirst().get();
    }

    public boolean hasUser(String name) {
        return this.loggedInUsers.stream().anyMatch(user -> user.getName().equals(name));
    }

    public void sendMessage(final String messageToSend, User user) {
        Session userSession = user.getSession();
        if (userSession.isOpen()) {
            try {
                userSession.getBasicRemote().sendObject(messageToSend);
            } catch (IOException | EncodeException ex) {
                Logger.getLogger(Room.class.getName()).log(SEVERE, null, ex);
            }
        }
    }

    public void sendHistoryToUser(User user) {
        chatHistory.getRecentHistory().forEach(message -> sendMessage(message.getJsonResponse().toString(), user));
    }

    public void receiveMessage(MessageRequest messageRequest, String messageAuthor) {
        MessageResponse messageResponse = new MessageResponse(messageAuthor, messageRequest.getMessage(), messageRequest.getReceived());

        broadcastResponse(messageResponse);
        storeMessage(messageResponse);
    }

    public void broadcastResponse(final ChatResponse chatResponse) {
        for (User user : this.loggedInUsers) {
            sendMessage(chatResponse.getJsonResponse().toString(), user);
        }
    }

    private void storeMessage(final MessageResponse chatMessage) {
        if (!chatMessage.getUsername().equals(CHAT_LOG_USERNAME)) {
            addToChatHistory(chatMessage);
            persistMessage(chatMessage);
        }
    }

    private void persistMessage(final MessageResponse chatMessage) {
        messageDao.addMessage(MessageResponseMapper.mapToEntity(chatMessage, roomName));
    }

    private void addToChatHistory(final MessageResponse chatMessage) {
        chatHistory.getHistory().add(chatMessage);

        if (chatHistory.getRecentHistory().size() > 100) chatHistory.getRecentHistory().poll();
        chatHistory.getRecentHistory().offer(chatMessage);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + Objects.hashCode(this.roomName);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Room other = (Room) obj;
        if (!Objects.equals(this.roomName, other.roomName)) {
            return false;
        }
        return true;
    }


}
