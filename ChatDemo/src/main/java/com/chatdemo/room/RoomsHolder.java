package com.chatdemo.room;

import javax.ejb.Singleton;
import javax.ejb.Startup;
import java.util.HashSet;
import java.util.Set;

@Startup
@Singleton
public class RoomsHolder {
    private final Set<Room> rooms = new HashSet<>();

    public RoomsHolder() {
        rooms.add(new Room("main"));
    }

    public Room getRoomFromName(String name) {
        return rooms.stream().filter(room -> room.getName().equals(name)).findFirst().get();
    }
}
