package com.chatdemo.room;

import javax.websocket.Session;
import java.io.Serializable;
import java.util.Objects;

public class User implements Serializable {
    private String name;
    private String email;
    private Session session;

    public User(String name, String email, Session session) {
        this.name = name;
        this.email = email;
        this.session = session;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 83 * hash + Objects.hashCode(this.name);
        hash = 83 * hash + Objects.hashCode(this.email);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final User other = (User) obj;
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.email, other.email)) {
            return false;
        }
        return true;
    }
}
