package com.chatdemo.room;

import com.chatdemo.communication.response.MessageResponse;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class ChatHistory {
    private final List<MessageResponse> history = new LinkedList<>();
    private final Queue<MessageResponse> recentHistory = new LinkedList<>();

    public List<MessageResponse> getHistory() {
        return history;
    }

    public Queue<MessageResponse> getRecentHistory() {
        return recentHistory;
    }
}
