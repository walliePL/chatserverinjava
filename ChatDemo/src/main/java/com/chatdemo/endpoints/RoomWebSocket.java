package com.chatdemo.endpoints;

import com.chatdemo.services.ChatRoomService;
import com.chatdemo.services.SendService;
import com.chatdemo.utils.ActionTypes;
import com.chatdemo.utils.exceptions.RoomServiceException;

import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.StringReader;
import java.util.logging.Level;
import java.util.logging.Logger;

import static java.util.logging.Level.SEVERE;

@ServerEndpoint("/rooms/{room}")
public class RoomWebSocket {
    private static final Logger log = Logger.getLogger(RoomWebSocket.class.getSimpleName());

    @Inject
    private ChatRoomService roomSrevice;

    @OnOpen
    public void open(Session session, @PathParam("room") final String room) {
        session.getUserProperties().put("room", room);
    }

    @OnClose
    public void close(Session session) {
        try {
            roomSrevice.logout(session);
        } catch (RoomServiceException roomServiceException) {
            log.warning(roomServiceException.getMessage());
        }
    }

    @OnError
    public void onError(Throwable error) {
        log.log(SEVERE, null, error);
    }

    @OnMessage
    public void onMessage(String message, Session session) {
        log.log(Level.INFO, "Message: {0}", message);
        try (JsonReader reader = Json.createReader(new StringReader(message))) {
            JsonObject jsonMessage = reader.readObject();

            String actionName;
            try {
                actionName = jsonMessage.getString("action");
            } catch (NullPointerException | ClassCastException ex) {
                SendService.sendErrorToSession(session, "Bad formatted request. Missing action in property");
                return;
            }

            try {
                processRequestAction(actionName, jsonMessage, session);
            } catch (IllegalArgumentException ex) {
                SendService.sendErrorToSession(session, "Malformed type of action property");
            }
        } catch (Exception e) {
            log.warning("Cannot parse given message to json.");
        }
    }

    private void processRequestAction(String actionName, JsonObject jsonMessage, Session session) {

        try {
            switch (ActionTypes.valueOf(actionName)) {
                case LOGIN:
                    roomSrevice.login(jsonMessage, session);
                    break;

                case LOGOUT:
                    roomSrevice.logout(session);
                    break;

                case MESSAGE:
                    roomSrevice.processChatMessage(session, jsonMessage);
                    break;

                default:
                    SendService.sendErrorToSession(session, "Not supported action in property");
            }
        } catch (RoomServiceException roomServiceException) {
            log.warning(roomServiceException.getMessage());
        }
    }
}
