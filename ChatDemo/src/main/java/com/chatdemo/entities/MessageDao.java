package com.chatdemo.entities;

import org.hibernate.Session;

public class MessageDao {

    public void addMessage(MessageEntity message) {
        Session session = DatabaseConnector.getSessionFactory().openSession();
        session.beginTransaction();

        session.save(message);

        session.getTransaction().commit();

        session.close();
    }
}
