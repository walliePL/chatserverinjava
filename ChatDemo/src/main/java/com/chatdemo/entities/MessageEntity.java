package com.chatdemo.entities;

import javax.persistence.*;
import java.util.Date;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "messages")
public class MessageEntity {
    @Id
    @Column(name = "message_id")
    @GeneratedValue(strategy = IDENTITY)
    private long id;
    @Column(name = "room_id")
    private String roomName;
    private String username;
    private String message;
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date received;

    public MessageEntity() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getReceived() {
        return received;
    }

    public void setReceived(Date received) {
        this.received = received;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }
}
