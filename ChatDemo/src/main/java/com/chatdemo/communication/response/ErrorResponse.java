package com.chatdemo.communication.response;

import javax.json.Json;
import javax.json.JsonObject;

public class ErrorResponse implements ChatResponse {
    private String errorMessage;

    public ErrorResponse(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    @Override
    public JsonObject getJsonResponse() {
        return Json.createObjectBuilder()
                .add("type", "Error")
                .add("errorMessage", errorMessage).build();
    }
}
