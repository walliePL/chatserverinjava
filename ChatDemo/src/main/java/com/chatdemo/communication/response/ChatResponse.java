package com.chatdemo.communication.response;

import javax.json.JsonObject;

public interface ChatResponse {
    JsonObject getJsonResponse();
}
