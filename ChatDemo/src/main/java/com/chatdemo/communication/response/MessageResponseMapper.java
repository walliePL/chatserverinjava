package com.chatdemo.communication.response;

import com.chatdemo.entities.MessageEntity;

public class MessageResponseMapper {
    public static MessageEntity mapToEntity(MessageResponse message, String roomName) {

        MessageEntity messageEntity = new MessageEntity();
        messageEntity.setMessage(message.getMessage());
        messageEntity.setUsername(message.getUsername());
        messageEntity.setRoomName(roomName);
        messageEntity.setReceived(message.getReceived());
        return messageEntity;
    }
}
