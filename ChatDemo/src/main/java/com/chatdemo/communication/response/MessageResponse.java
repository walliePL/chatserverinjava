package com.chatdemo.communication.response;

import javax.json.Json;
import javax.json.JsonObject;
import java.util.Date;

public class MessageResponse implements ChatResponse {
    private String username;
    private String message;
    private Date received;

    public MessageResponse(String username, String message, Date received) {
        this.username = username;
        this.message = message;
        this.received = received;
    }

    public String getUsername() {
        return username;
    }

    public String getMessage() {
        return message;
    }

    public Date getReceived() {
        return received;
    }

    @Override
    public JsonObject getJsonResponse() {
        return Json.createObjectBuilder()
                .add("type", "Message")
                .add("message", message)
                .add("username", username)
                .add("received", received.toString()).build();
    }
}
