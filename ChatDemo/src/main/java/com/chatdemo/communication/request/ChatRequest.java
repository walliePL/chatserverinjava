package com.chatdemo.communication.request;

import javax.json.JsonObject;

public interface ChatRequest {
    void mapRequest(JsonObject request);
}
