package com.chatdemo.communication.request;

import javax.json.JsonObject;
import java.util.Date;

public class MessageRequest implements ChatRequest {

    private String message;
    private Date received;

    public String getMessage() {
        return message;
    }

    public Date getReceived() {
        return received;
    }

    @Override
    public void mapRequest(JsonObject request) {
        message = request.getString("message");
        received = new Date();
    }

}

