package com.chatdemo.utils;

public enum ActionTypes {
    LOGIN,
    LOGOUT,
    MESSAGE
}
