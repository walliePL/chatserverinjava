
package com.chatdemo.utils.exceptions;

public class RoomServiceException extends Exception {

    public RoomServiceException() {
    }

    public RoomServiceException(String msg) {
        super(msg);
    }
}
