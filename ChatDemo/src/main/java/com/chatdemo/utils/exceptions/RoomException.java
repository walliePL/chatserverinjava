package com.chatdemo.utils.exceptions;

public class RoomException extends Exception {

    public RoomException() {
    }
    
    public RoomException(String msg) {
        super(msg);
    }
}
